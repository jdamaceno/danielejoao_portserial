//////////////////////////////////////////////////////////////////////
// File Downloaded from http://www.nandland.com
//////////////////////////////////////////////////////////////////////
`timescale 1ns/10ps

 
module top_rx (output [7:0] w_Rx_Byte
					     );
 
  // Testbench uses a 10 MHz clock
  // Want to interface to 115200 baud UART
  // 10000000 / 115200 = 87 Clocks Per Bit.
  parameter c_CLOCK_PERIOD_NS = 100;
  parameter c_CLKS_PER_BIT    = 87;
  parameter c_BIT_PERIOD      = 8600;
   
  reg r_Clock = 0;
  reg UART_RXD;
  wire stop_RX;
	
		  // Takes in input byte and serializes it 
  task UART_WRITE_BYTE;
    input [7:0] i_Data;
    integer     ii;
    begin
       
      // Send Start Bit
      UART_RXD <= 1'b0;
        #(c_BIT_PERIOD);
       
       
      // Send Data Byte
      for (ii=0; ii<8; ii=ii+1)
        begin
          UART_RXD <= i_Data[ii];
          #(c_BIT_PERIOD);
        end
       
      // Send Stop Bit
      UART_RXD <= 1'b1;
      #(c_BIT_PERIOD);
     end
  endtask // UART_WRITE_BYTE
	
	
  uart_rx #(.CLKS_PER_BIT(c_CLKS_PER_BIT)) UART_RX_INST
    (.i_Clock(r_Clock),
     .i_Rx_Serial(UART_RXD), //PINO RX
     .o_Rx_DV(stop_RX),
     .o_Rx_Byte(w_Rx_Byte)
     );
 
   
  always
    #(c_CLOCK_PERIOD_NS/2) r_Clock <= !r_Clock;
 
 	always
	 begin
		 
		// Send a command to the UART (exercise Rx)
		@(posedge r_Clock);
		UART_WRITE_BYTE(8'b0000_0001);
		@(posedge r_Clock);
		
		#(100)
		 
		@(posedge r_Clock);
		UART_WRITE_BYTE(8'b0000_0010);
		@(posedge r_Clock);
	 end
   
endmodule
