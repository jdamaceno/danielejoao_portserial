vlib work
vmap work work
vlog *.sv
vlog *.v
vsim mipstest

add wave -radix hexadecimal sim:/mipstest/dut/mips/*
add wave -radix hexadecimal sim:/mipstest/dut/rx/*

run 1000000ns