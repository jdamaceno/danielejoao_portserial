**************************************
*********MIPS com SERIAL**************
**************************************

Nesta vers�o foi adicionado o c�digo encontrado no site:

https://www.nandland.com/vhdl/modules/module-uart-serial-port-rs232.html

Com algumas altera��es quanto � sa�da e o teste, em teste foi colocado para
se repetir a recep��o de 1 e 2 em loop. Portanto, sempre recebendo os valores passados no
teste.

Foi implementado a instru��o para escrever o conte�do da sa�da do modulo Top_Rx no segundo registrador.
A opera��o � do Tipo R portanto ficando com o seguinte formato 000000_00000_WRREG_00000_001000.
Sendo WRREG o registrador em que ser� escrito o valor que se encontra na sa�da do modulo top_rx.

Quanto ao programa de instru��es foi feito um simples loop que l� o registrador em que � escrito o valor de RX, 
caso o mesmo seja 1 ou 2 entra em um dos casos que apenas faz uma soma simples por fins de teste, e ent�o volta para 
o loop que fica recebendo o valor do registrador. Para mais detalhes veja o c�digo no arquivo "memfile.asm".

Uma vis�o geral do circuito pode ser visualizada no arquivo "TOP_VIEW.PNG".

Autores:
	Daniel Lamounier Heringer
	Jo�o Gabriel Damasceno